from dataclasses import dataclass

@dataclass
class Pool:
    def __init__(self, x: float, y: float, fee_percent: float):
        self.x = x
        self.y = y
        self.fee_percent = fee_percent
    
    def k(self) -> float:
        return self.x * self.y

    def fee_factor(self) -> float:
        return 1 - self.fee_percent / 100

    def x_spot_price(self) -> float:
        return self.y / self.x

    def y_spot_price(self) -> float:
        return self.x / self.y

    def swap_x_for_y(self, delta_x: float) -> float:
        fee_adjusted_x = delta_x * self.fee_factor()
        delta_y = self.y - (self.x * self.y) / (self.x + fee_adjusted_x)
        self.x += delta_x
        self.y -= delta_y
        return delta_y

    def swap_y_for_x(self, delta_y: float) -> float:
        fee_adjusted_y = delta_y * self.fee_factor()
        delta_x = self.x - (self.x * self.y) / (self.y + fee_adjusted_y)
        self.x -= delta_x
        self.y += delta_y
        return delta_x

